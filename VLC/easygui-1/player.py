import vlc, easygui
media = easygui.fileopenbox(title="Choisissez le média(audio/vidéo)")
player = vlc.MediaPlayer(media)
image = "radio.gif"
while True:
    choice = easygui.buttonbox(title="Audio Player",msg="Appuyez sur Play pour commencer",image=image,
											choices=["Play","Pause","Stop","Nouveau"])
    print(choice)
    if choice == "Play":
        player.play()
    elif choice == "Pause":
        player.pause()
    elif choice == "Stop":
        player.stop()
    elif choice == "Nouveau":
        media = easygui.fileopenbox(title="Choisissez le média(audio/vidéo)")
        player = vlc.MediaPlayer(media)
    else:
        break
