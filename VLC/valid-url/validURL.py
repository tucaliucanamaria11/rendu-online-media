import vlc  # module vlc
import time # pour la fonction sleep()

url = 'https://cerisefm.ice.infomaniak.ch/cerisefm-128.mp3'
# définir l'instance VLC
instance = vlc.Instance('--input-repeat=-1', '--fullscreen')
# définir le VLC player
player=instance.media_player_new()
# définir VLC media
media=instance.media_new(url)
# le player media
player.set_media(media)
# on joue le flux
player.play()

# 5 secondes pour que VLC termine les tentatives.
time.sleep(5)
# l'état courant
state = str(player.get_state())

# message pour vérifier si le flux est valide
if state == "vlc.State.Error" or state == "State.Error":
    print ('URL broken. Current state = {}'.format(state))
    player.stop()
else:
    print ('URL valid. Current state = {}'.format(state))
    player.stop()
