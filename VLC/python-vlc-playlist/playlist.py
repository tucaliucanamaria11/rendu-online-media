# script python pour boucler sur 4 flux radio
# install >>> pip install python-vlc
import vlc
import time


playlist = ['http://streaming.radio.rtl2.fr/rtl2-1-44-128', 'http://stream.generation-fm.com/generationfm-128.mp3',
 'http://alpes1gap.ice.infomaniak.ch/alpes1gap-high.mp3', 'http://bfmbusiness.cdn.dvmr.fr/bfmbusiness']

# on boucle sur les 4 flux audio dans notre playlist
for radio in playlist:
    player = vlc.MediaPlayer(radio)
    player.play()
    time.sleep(100)
    player.stop()
