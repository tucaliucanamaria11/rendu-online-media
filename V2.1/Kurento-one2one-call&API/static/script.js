// on récupère la video par son id, ça sera la tag de la vidéo
const video = document.getElementById('videoOutput')

// éxecuter ne parallèle les 4 différents modelès (tinyFaceDetector, faceLandmark68Net, faceRecognitionNet, faceExpressionNet)
// >>>  on fait l'appel depuis faceapi
Promise.all([
  faceapi.nets.tinyFaceDetector.loadFromUri('/models'),
  faceapi.nets.faceLandmark68Net.loadFromUri('/models'), // enregistrer les différents parties du visage (bouche, nez, yeux etc.)
  faceapi.nets.faceRecognitionNet.loadFromUri('/models'), // trouver le visage par une boîte
  faceapi.nets.faceExpressionNet.loadFromUri('/models') // trouver les émotions (sourire, triste etc. )
]).then(startVideo) // une fois l'éxécution est finie on appelle la fonction startVideo()

// fonction pour commencer la vidéo via notre webcam
function startVideo() {
  // on récupère le flux de la webcam
  navigator.mediaDevices.getUserMedia(
    { video: {} },
    stream => video.srcObject = stream, // l'entrée de la caméra = source de la vidéo
    err => console.error(err)           // afficher l'erreur si jamais
  )
}

// un événement écoute, quand la vidéo commence
video.addEventListener('play', () => {
  console.log("coucou test");
  // pour l'affichage à l'écran on utilise un canvas crée depuis notre flux vidéo
  const canvas = faceapi.createCanvasFromMedia(video)
  // on ajoute le canvas à l'écran
  document.body.append(canvas)
  // les dimensions pour que le canvas soit au-dessous de notre vidéo
  const displaySize = { width: video.width, height: video.height }
  faceapi.matchDimensions(canvas, displaySize)
  // on éxécute le code dedans plusieurs fois
  // bibliothéque async
  setInterval(async () => {
    // detectAllFaces donne tous les visages trouvés dans la webcam pour chaque 100 milisecondes
    // comme arguments on lui passe : l'élément vidéo et le type de bibliothéque qu'on va utiliser dans le detecteur
    // withFaceLandmarks() - trouver le visage, avec des pointiles
    // withFaceExpressions() - trouver les expressions faciales(heureux, triste etc...)
    const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceExpressions()
    // visualiser dans la console
    console.log(detections);
    const resizedDetections = faceapi.resizeResults(detections, displaySize) // pour bien avoir la taille correspondante au visage
    // avant de dessiner on efface le canvas
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
    faceapi.draw.drawDetections(canvas, resizedDetections)
    faceapi.draw.drawFaceLandmarks(canvas, resizedDetections) // ne nez, bouche etc.
    faceapi.draw.drawFaceExpressions(canvas, resizedDetections)
  }, 100)
})
