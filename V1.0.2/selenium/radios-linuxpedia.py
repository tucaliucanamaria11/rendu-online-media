# pas d'interface graphique -> automatisation chercher l'information depuis le serveru fournisseur
# améliore les performances, on a un bot et pas de client, problème en temps réel car beaucoup de requetes(5000 requetes par secondes)
# exécution avec python3 radios-linuxpedia.py
from selenium import webdriver
# pour le fichier json
import json
driver = webdriver.Firefox() 
driver.get('https://www.linuxpedia.fr/doku.php/flux_radio')
# récupere des différents éléments par exemple >>> element = driver.find_element_by_xpath('//*[@id="chiffres"]') + afficher avec >>> print(element.text)
elts = driver.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[2]/div')

# Récupérer les elements de taille h2 
elts_cat = driver.find_element_by_xpath('/html/body/div[1]/div/div[2]/div[2]/div')
elts_name_cat=elts_cat.find_elements_by_tag_name("h2") # on a les éléments sous forme de liste elts_name_cat[i].text

# Recupérer les donnes des tableaux
elts_data = driver.find_element_by_xpath("/html/body/div[1]/div/div[2]/div[2]/div/div[2]")

# Recupérer les titres de radios
elts_titles = elts_data.find_elements_by_class_name("col0") # une liste >> les éléments sont elts_titles[i].text

# Récupérer les liens de flux radios
elts_links = elts_data.find_elements_by_class_name("col1") # liste les éléments sont de la forme elts_links[i].text 

# Mettre en forme (en JSON)
data = {}
data['flux-linuxpedia']= []
for i in range(len(elts_titles)):
    name = elts_titles[i].text
    link = elts_links[i].text
    data['flux-linuxpedia'].append({
        'nom-radio': name ,
        'flux' : link
    })

name_file = 'radios-linuxpedia.json'
with open(name_file, 'w') as outfile:
    json.dump(data, outfile)

# on ferme le webdriver
driver.close()
